package impl;

public class Habits {
	
	public String smoking= "No";
	public String alcohol= "Yes";
	public String dailyExcersice= "Yes";
	public String drugs= "No";
	public String getSmoking() {
		return smoking;
	}
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}
	public String getAlcohol() {
		return alcohol;
	}
	public void setAlcohol(String alcohol) {
		this.alcohol = alcohol;
	}
	public String getDailyExcersice() {
		return dailyExcersice;
	}
	public void setDailyExcersice(String dailyExcersice) {
		this.dailyExcersice = dailyExcersice;
	}
	public String getDrugs() {
		return drugs;
	}
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}
	
	

}
