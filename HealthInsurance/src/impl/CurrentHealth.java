package impl;

public class CurrentHealth {
	
	public String hypertension = "No";
	public String bloodPressure= "No";
	public String bloodSugar= "No";
	public String overWeight= "Yes";
	public String getHypertension() {
		return hypertension;
	}
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}
	public String getBloodPressure() {
		return bloodPressure;
	}
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}
	public String getBloodSugar() {
		return bloodSugar;
	}
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}
	public String getOverWeight() {
		return overWeight;
	}
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

}
